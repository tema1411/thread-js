import { Router } from 'express';
import * as commentService from '../services/commentService';

const router = Router();

router
  .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .get('/likes/:id', (req, res, next) => commentService.getLikesById(req.params.id)
    .then(likes => res.send(likes))
    .catch(next))
  .delete('/:id', (req, res, next) => commentService.deleteCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .put('/', (req, res, next) => commentService.updateCommentById(req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
    .then(reaction => {
      // if (reaction.post && (reaction.post.userId !== req.user.id)) {
      //   // notify a user if someone (not himself) liked his post
      //   req.io.to(reaction.post.userId).emit('like', 'Your post was liked!');
      // }
      console.log('reaction', reaction);
      return res.send(reaction);
    })
    .catch(next));

export default router;
