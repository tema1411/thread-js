import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);
export const getWhoLikedPostById = id => postReactionRepository.getWhoSetEmotionPostById(id, true);
export const getWhoDislikedPostById = id => postReactionRepository.getWhoSetEmotionPostById(id, false);
export const deletePostById = id => postRepository.deletePostById(id);
export const updatePostById = postData => {
  const { id: commentId, data: newCommentData } = postData;
  return postRepository.updatePostById(commentId, newCommentData);
};
export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const setReaction = async (userId, { postId, isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike }));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};
