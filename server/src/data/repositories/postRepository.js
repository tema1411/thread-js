import sequelize from '../db/connection';

import {
  PostModel,
  CommentModel,
  UserModel,
  ImageModel,
  PostReactionModel,
  CommentReactionModel
} from '../models/index';
import BaseRepository from './baseRepository';

const { Op } = require('sequelize');

const likeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;
const commentLikeCase = bool => `(SELECT
   COALESCE(SUM(CASE WHEN "commentReactions"."isLike"=${bool} THEN 1 ELSE 0 END),0)
   FROM "commentReactions"
   WHERE "commentId" = "comments"."id")`;

class PostRepository extends BaseRepository {
  updatePostById(id, data) {
    return this.updateById(id, data);
  }

  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId,
      withoutUserId
    } = filter;
    const where = {
      isShow: true
    };

    if (withoutUserId) {
      Object.assign(where, { userId: { [Op.not]: userId } });
    } else if (userId) {
      Object.assign(where, { userId });
    }

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."isShow" = true)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: PostReactionModel,
        attributes: [],
        duplicating: false
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  deletePostById(id) {
    return this.updateById(id, { isShow: false });
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."isShow" = true)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: CommentModel,
        where: {
          isShow: true
        },
        required: false,
        attributes: {
          include: [
            [sequelize.literal(commentLikeCase(true)), 'likeCount'],
            [sequelize.literal(commentLikeCase(false)), 'dislikeCount']
          ]
        },
        include: [
          {
            model: UserModel,
            attributes: ['id', 'username'],
            include: {
              model: ImageModel,
              attributes: ['id', 'link']
            }
          }
        ]
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: PostReactionModel,
        attributes: []
      }, {
        model: CommentReactionModel,
        attributes: []
      }]
    });
  }
}

export default new PostRepository(PostModel);
