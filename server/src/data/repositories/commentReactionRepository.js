import { CommentReactionModel } from '../models/index';
import BaseRepository from './baseRepository';

class CommentReactionRepository extends BaseRepository {
  getCommentReaction(userId, postId, commentId) {
    return this.model.findOne({
      // group: [
      //   'postReaction.id',
      //   'post.id'
      // ],
      where: {
        userId,
        postId,
        commentId
      }
    });
  }
}

export default new CommentReactionRepository(CommentReactionModel);
