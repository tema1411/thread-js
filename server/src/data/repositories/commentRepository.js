import sequelize from '../db/connection';
import { CommentModel, UserModel, ImageModel, CommentReactionModel } from '../models/index';
import BaseRepository from './baseRepository';

const likeCase = bool => `CASE WHEN "commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class CommentRepository extends BaseRepository {
  deleteCommentById(id) {
    return this.updateById(id, { isShow: false });
  }

  updateCommentById(id, data) {
    return this.updateById(id, data);
  }

  getLikesByCommentId(id) {
    return this.model.findOne({
      where: { id }
    });
  }

  getCommentById(id) {
    return this.model.findOne({
      group: [
        'comment.id',
        'user.id',
        'commentReactions.id',
        'user->image.id'
      ],
      where: { id, isShow: true },
      include: [
        {
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        },
        {
          model: CommentReactionModel,
          attributes: [
            [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
            [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
          ]
        }
      ]
    });
  }
}

export default new CommentRepository(CommentModel);
