import { PostReactionModel, PostModel, UserModel, ImageModel } from '../models/index';
import BaseRepository from './baseRepository';

class PostReactionRepository extends BaseRepository {
  getWhoSetEmotionPostById(id, isLike) {
    return this.model.findAll({
      where: { postId: id, isLike },
      attributes: [],
      include: [
        {
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['link']
          }
        }
      ]
    });
  }

  getPostReaction(userId, postId) {
    return this.model.findOne({
      group: [
        'postReaction.id',
        'post.id'
      ],
      where: { userId, postId },
      include: [{
        model: PostModel,
        attributes: ['id', 'userId']
      }]
    });
  }
}

export default new PostReactionRepository(PostReactionModel);
