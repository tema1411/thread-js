module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.addColumn(
    'posts',
    'isShow',
    {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    }
  ),

  down: queryInterface => queryInterface.removeColumn('posts', 'isShow')
};
