module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.addColumn(
    'comments',
    'isShow',
    {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    }
  ),

  down: queryInterface => queryInterface.removeColumn('comments', 'isShow')
};
