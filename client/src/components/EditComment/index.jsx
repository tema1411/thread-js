import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Dimmer, Loader } from 'semantic-ui-react';

const { TextArea } = Form;

const EditContent = ({ currentText, label, cancel, update, setIsEdit }) => {
  const [text, setText] = useState(currentText);
  const [isLoading, setIsLoading] = useState(false);

  const submitHandler = () => {
    if (!text) {
      return;
    }

    setIsLoading(true);

    update(text)
      .then(() => {
        setIsEdit(false);
      })
      .catch(error => {
        alert(error);
        setIsLoading(false);
      });
  };

  return (
    <Form onSubmit={submitHandler}>
      <Dimmer active={isLoading} inverted>
        <Loader />
      </Dimmer>
      <TextArea
        label={label}
        onChange={e => setText(e.target.value)}
        value={text}
      />
      <Button loading={isLoading} type="submit" content="Update" icon="edit" primary />
      <Button type="button" content="Cancel" icon="cancel" onClick={cancel} />
    </Form>
  );
};

EditContent.propTypes = {
  currentText: PropTypes.string.isRequired,
  cancel: PropTypes.func.isRequired,
  update: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  setIsEdit: PropTypes.func.isRequired
};

export default EditContent;
