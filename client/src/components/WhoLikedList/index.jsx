// eslint-disable-next-line no-unused-vars
import React, { useState, useEffect } from 'react';
import { List, Image, Placeholder, Card } from 'semantic-ui-react';
import { getUserImgLink } from 'src/helpers/imageHelper';
import PropTypes from 'prop-types';
import style from './styles.module.scss';

const Wrapper = ({ children }) => (
  <Card className={style.container}>
    <Card.Content>
      {children}
    </Card.Content>
  </Card>
);

Wrapper.propTypes = {
  children: PropTypes.element.isRequired
};

const WhoLikedList = ({ getFunction, id: postId }) => {
  const [users, setUsers] = useState(null);

  useEffect(() => {
    async function load() {
      const whoEmotionList = await getFunction(postId);
      setUsers(whoEmotionList);
    }

    load();
  }, [getFunction, postId]);

  if (users === null) {
    return (
      <Wrapper>
        <Placeholder>
          <Placeholder.Header image>
            <Placeholder.Line />
            <Placeholder.Line />
          </Placeholder.Header>
          <Placeholder.Header image>
            <Placeholder.Line />
            <Placeholder.Line />
          </Placeholder.Header>
        </Placeholder>
      </Wrapper>

    );
  }

  if (users.length) {
    return (
      <Wrapper>
        <List>
          {users.map(({ user: { id, username, image } }) => (
            <List.Item key={id}>
              <Image avatar src={getUserImgLink(image)} />
              <List.Content>
                <List.Header>{username}</List.Header>
              </List.Content>
            </List.Item>
          ))}
        </List>
      </Wrapper>
    );
  }
  return (
    <Wrapper>
      <p>Empty</p>
    </Wrapper>
  );
};

WhoLikedList.propTypes = {
  getFunction: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired
};

export default WhoLikedList;
