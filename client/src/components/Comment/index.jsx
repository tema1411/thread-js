import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import EditContent from '../EditComment';
import styles from './styles.module.scss';

const Comment = React.memo(({ comment, currentUserId, likeComment, dislikeComment, deleteComment, updateComment }) => {
  const { body, createdAt, user, id: commentId, postId, likeCount, dislikeCount } = comment;
  const [isEdit, setIsEdit] = useState(false);

  const handlerUpdateComment = newBody => updateComment(commentId, { body: newBody });

  const commentAction = [
    <CommentUI.Action onClick={() => likeComment(postId, commentId)} key={1}>
      <Icon name="thumbs up" />
      {likeCount}
    </CommentUI.Action>,
    <CommentUI.Action onClick={() => dislikeComment(postId, commentId)} key={2}>
      <Icon name="thumbs down" />
      {dislikeCount}
    </CommentUI.Action>
  ];

  if (user.id === currentUserId) {
    commentAction.push(
      <CommentUI.Action onClick={() => setIsEdit(true)} key={3}>
        <Icon name="pencil" />
      </CommentUI.Action>,
      <CommentUI.Action onClick={() => deleteComment(postId, commentId)} key={4}>
        <Icon name="remove" />
      </CommentUI.Action>
    );
  }

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt)
            .fromNow()}
        </CommentUI.Metadata>
        <CommentUI.Text>
          {body}
        </CommentUI.Text>
        {isEdit && (
          <EditContent
            currentText={body}
            setIsEdit={setIsEdit}
            label="Edit Comment"
            cancel={() => setIsEdit(false)}
            update={handlerUpdateComment}
          />
        )}
        <CommentUI.Actions>
          {!isEdit && commentAction}
        </CommentUI.Actions>
      </CommentUI.Content>
    </CommentUI>
  );
});

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  currentUserId: PropTypes.string.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired
};

export default Comment;
