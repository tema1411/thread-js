import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';

import styles from './styles.module.scss';
import EditContent from '../EditComment';
import WhoLikedList from '../WhoLikedList';
import { getWhoLiked, getWhoDisliked } from '../../services/postService';

const Post = props => {
  const {
    post,
    likePost,
    dislikePost,
    deletePost,
    toggleExpandedPost,
    sharePost,
    currentUserId,
    updatePost
  } = props;

  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = moment(createdAt).fromNow();
  const [isShowLike, setIsShowLike] = useState(false);
  const [isShowDislike, setIsShowDislike] = useState(false);

  const [isEdit, setIsEdit] = useState(false);
  const handlerUpdatePost = newBody => updatePost(id, { body: newBody });

  const postAction = [
    <div
      className={styles.emotionContainer}
      onMouseMove={() => setIsShowLike(true)}
      onMouseLeave={() => setIsShowLike(false)}
      onFocus={() => setIsShowLike(true)}
      onBlur={() => setIsShowLike(false)}
      key={1}
    >
      {isShowLike && <WhoLikedList getFunction={getWhoLiked} id={id} /> }
      <Label
        basic
        size="small"
        as="a"
        className={styles.toolbarBtn}
        onClick={() => { setIsShowLike(false); likePost(id); }}
      >
        <Icon name="thumbs up" />
        {likeCount}
      </Label>
    </div>,
    <div
      className={styles.emotionContainer}
      onMouseMove={() => setIsShowDislike(true)}
      onMouseLeave={() => setIsShowDislike(false)}
      onFocus={() => setIsShowDislike(true)}
      onBlur={() => setIsShowDislike(false)}
      key={2}
    >
      {isShowDislike && <WhoLikedList getFunction={getWhoDisliked} id={id} /> }
      <Label
        basic
        size="small"
        as="a"
        className={styles.toolbarBtn}
        onClick={() => { setIsShowDislike(false); dislikePost(id); }}
      >
        <Icon name="thumbs down" />
        {dislikeCount}
      </Label>
    </div>,
    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)} key={3}>
      <Icon name="comment" />
      {commentCount}
    </Label>,
    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)} key={4}>
      <Icon name="share alternate" />
    </Label>
  ];

  if (user.id === currentUserId) {
    postAction.push(
      <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => setIsEdit(true)} key={5}>
        <Icon name="pencil alternate" />
      </Label>,
      <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => deletePost(id)} key={6}>
        <Icon name="remove" />
      </Label>
    );
  }

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
        {isEdit && (
          <EditContent
            currentText={body}
            setIsEdit={setIsEdit}
            label="Edit post"
            cancel={() => setIsEdit(false)}
            update={handlerUpdatePost}
          />
        )}
      </Card.Content>
      <Card.Content extra>
        {!isEdit && postAction}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  currentUserId: PropTypes.string.isRequired,
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired
};

export default Post;
