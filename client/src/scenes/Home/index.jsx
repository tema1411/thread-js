import React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import Routing from 'src/containers/Routing';
import store, { history } from 'src/store';
import { SemanticToastContainer } from 'react-semantic-toasts';
import 'react-semantic-toasts/styles/react-semantic-alert.css';

const Home = () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Routing />
      <SemanticToastContainer />
    </ConnectedRouter>
  </Provider>
);

export default Home;
