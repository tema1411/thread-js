import callWebApi from 'src/helpers/webApiHelper';

export const addComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments',
    type: 'POST',
    request
  });
  return response.json();
};

export const likeComment = async (postId, commentId) => {
  const response = await callWebApi({
    endpoint: '/api/comments/react',
    type: 'PUT',
    request: {
      postId,
      commentId,
      isLike: true
    }
  });
  return response.json();
};

export const changeComment = async (id, data) => {
  const response = await callWebApi({
    endpoint: '/api/comments',
    type: 'PUT',
    request: {
      id,
      data
    }
  });
  return response.json();
};

export const deleteComment = async id => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'DELETE'
  });

  return response.json();
};

export const dislikeComment = async (postId, commentId) => {
  const response = await callWebApi({
    endpoint: '/api/comments/react',
    type: 'PUT',
    request: {
      postId,
      commentId,
      isLike: false
    }
  });
  return response.json();
};

export const getComment = async id => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'GET'
  });
  return response.json();
};
