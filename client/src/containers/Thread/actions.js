import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import moment from 'moment';

import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const updatePost = (postId, data) => async (dispatch, getRootState) => {
  const { body } = await postService.updatePost(postId, data);
  if (!body) throw new Error('Ooops. Try please later');

  const { posts: { posts, expandedPost } } = getRootState();

  const getUpdatePost = post => ({
    ...post,
    body
  });

  if (!body) throw new Error('Ooops. Try please later');

  const updatedPosts = posts.map(post => (post.id === postId ? getUpdatePost(post) : post));
  dispatch(setPostsAction(updatedPosts));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(getUpdatePost(expandedPost)));
  }
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const likePost = postId => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await postService.likePost(postId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  // if createdAt is not same updatedAt then the post was disliked, otherwise - dislike was removed
  const diffDislike = moment(createdAt)
    .isSame(updatedAt) ? 0 : -1;

  const mapLikes = post => ({
    ...post,
    dislikeCount: Number(post.dislikeCount) + diffDislike,
    likeCount: Number(post.likeCount) + diff // diff is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await postService.dislikePost(postId);
  const diff = id ? 1 : -1; // if ID exists then the post was disliked, otherwise - dislike was removed
  // if createdAt is not same updatedAt then the post was liked, otherwise - like was removed
  const diffLike = moment(createdAt)
    .isSame(updatedAt) ? 0 : -1;

  const mapDisLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diffLike,
    dislikeCount: Number(post.dislikeCount) + diff // diff is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDisLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDisLikes(expandedPost)));
  }
};

export const deletePost = postId => async (dispatch, getRootState) => {
  const { id } = await postService.deletePost(postId);
  if (id) {
    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.filter(post => post.id !== postId);

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
      dispatch(setExpandedPostAction());
    }
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const deleteComment = (postId, commentId) => async (dispatch, getRootState) => {
  const { id } = await commentService.deleteComment(commentId);

  if (id) {
    const getUpdatedComments = comments => comments.filter(comment => comment.id !== commentId);

    const mapComments = post => ({
      ...post,
      commentCount: Number(post.commentCount) <= 0 ? Number(post.commentCount) : Number(post.commentCount) - 1,
      comments: getUpdatedComments(post.comments) // comment is taken from the current closure
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== postId ? post : mapComments(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
      dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
  }
};

export const likeComment = (postId, commentId) => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await commentService.likeComment(postId, commentId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

  // if createdAt is not same updatedAt then the post was disliked, otherwise - dislike was removed
  const diffDislike = moment(createdAt)
    .isSame(updatedAt) ? 0 : -1;

  const mapLikes = comment => ({
    ...comment,
    dislikeCount: Number(comment.dislikeCount) + diffDislike,
    likeCount: Number(comment.likeCount) + diff // diff is taken from the current closure
  });

  const { posts: { expandedPost } } = getRootState();
  const { comments } = expandedPost;

  if (expandedPost) {
    const updatedComments = comments.map(comment => (comment.id !== commentId
      ? comment
      : mapLikes(comment)));

    const updatedExpandedPost = { ...expandedPost, comments: updatedComments };

    dispatch(setExpandedPostAction(updatedExpandedPost));
  }
};

export const dislikeComment = (postId, commentId) => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await commentService.dislikeComment(postId, commentId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

  // if createdAt is not same updatedAt then the post was disliked, otherwise - dislike was removed
  const diffLike = moment(createdAt)
    .isSame(updatedAt) ? 0 : -1;

  const mapLikes = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + diffLike,
    dislikeCount: Number(comment.dislikeCount) + diff
  });

  const { posts: { expandedPost } } = getRootState();

  if (expandedPost) {
    const { comments } = expandedPost;
    const updatedComments = comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)));

    const updatedExpandedPost = { ...expandedPost, comments: updatedComments };

    dispatch(setExpandedPostAction(updatedExpandedPost));
  }
};

export const changeComment = (commentId, commentData) => async (dispatch, getRootState) => {
  const { body } = await commentService.changeComment(commentId, commentData);
  if (!body) throw new Error('Ooops. Try please later');

  const { posts: { expandedPost } } = getRootState();

  const getUpdateComment = comment => ({
    ...comment,
    body
  });

  if (expandedPost) {
    const { comments } = expandedPost;
    const updatedComments = comments.map(comment => (comment.id === commentId ? getUpdateComment(comment) : comment));

    const updatedExpandedPost = { ...expandedPost, comments: updatedComments };
    dispatch(setExpandedPostAction(updatedExpandedPost));
  }
};
